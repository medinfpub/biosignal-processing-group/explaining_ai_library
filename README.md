# Explaining_AI_library
This repo contains the recent version of the Explaining AI (XAI) software that is developed and maintained by Philip Hempel -PhD student of the Biosignal Research Group of the University Medical Center Göttingen.

While the publication as python package is planed, this repo will still serve as distribution point w.r.t. publication of scientific work related to this software library.

The library based on the open source ECG-plot python package by dy1901: https://github.com/dy1901/ecg_plot

The shown example based on an open source AI-ECG system developed by Antônio Riberio:
https://github.com/antonior92/automatic-ecg-diagnosis
publicated in https://www.nature.com/articles/s41467-020-15432-4

The used dataset and feature dataset has been published by Nils Strodthoff:
https://www.physionet.org/content/ptb-xl-plus/1.0.1/ publicated in https://www.nature.com/articles/s41597-020-0495-6


![Example for visualizing XAI for TP detection of LBBB](example_LBBB.png)
ECG showing a left bundle branch block (LBBB) diagnosed by both a cardiologist and the AI-ECG system (True Positive). The pink colors indicate the regions that are most important for AI decision making.

![Example for visualizing XAI for TP detection of no LBBB](example_noLBBB.png)
ECG that has been diagnosed as not LBBB by both cardiologists and the AI-ECG system (True Negative). The teal color highlights the regions that are most relevant for the given diagnosis of the AI system “no LBBB”. 
