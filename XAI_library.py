import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from math import ceil


def _ax_plot(ax, x, y, secs=10, lwidth=0.5, amplitude_ecg = 1.8, time_ticks =0.2):
    ax.set_xticks(np.arange(0,11,time_ticks))
    ax.set_yticks(np.arange(-ceil(amplitude_ecg),ceil(amplitude_ecg),1.0))

    #ax.set_yticklabels([])
    #ax.set_xticklabels([])

    ax.minorticks_on()

    ax.xaxis.set_minor_locator(AutoMinorLocator(5))

    ax.set_ylim(-amplitude_ecg, amplitude_ecg)
    ax.set_xlim(0, secs)

    ax.grid(which='major', linestyle='-', linewidth='0.5', color='red')
    ax.grid(which='minor', linestyle='-', linewidth='0.5', color=(1, 0.7, 0.7))

    ax.plot(x,y, linewidth=lwidth)


lead_index = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']

def save_as_png(file_name, path, dpi = 200, layout='tight'):
    """Plot multi lead ECG chart.
    # Arguments
        file_name: file_name
        path     : path to save image, defaults to current folder
        dpi      : set dots per inch (dpi) for the saved image
        layout   : Set equal to "tight" to include ax labels on saved image
    """
    plt.ioff()
    plt.savefig(os.path.join(path, file_name) + '.png', dpi = dpi, bbox_inches=layout)
    plt.close()

def plot(
        ecg,
        sample_rate    = 500,
        title          = 'ECG 12',
        XAI            = None,
        XAI_threshold  = 0,
        XAI_normalize  = True,
        XAI_quartile   = 0.3,
        XAI_show_just  = None,
        XAI_colors     = None,
        lead_index     = lead_index,
        lead_order     = None,
        style          = None,
        columns        = 2,
        row_height     = 6,
        show_lead_name = True,
        show_grid      = True,
        show_separate_line  = True,
        ):
    """Plot multi lead ECG chart.
    # Arguments
        ecg        : m x n ECG signal data, which m is number of leads and n is length of signal.
        sample_rate: Sample rate of the signal.
        title      : Title which will be shown on top off chart
        XAI        : Xplainable AI methods can be added to visualize the model's decision;
                     shape has to be the identical to 'ecg'
        XAI_normalize: Normalizes values between 0 and 1 for for positive relevances and -1 and 1 for relevances
                       with positive and negative values
        XAI_quartile: Quartile to spare for normalization; default to 0.03 (otherwise relevances are to small); 0 for min/max normalization
        XAI_show_just: Can be used to show only positive or negative relevances;
                       can be 'positive'/1 or 'negative'
        XAI_colors : Defaul is teal/ rosa; can be an integer between 1 and 4; or a list with two colors ['color1', 'color2']
        lead_index : Lead name array in the same order of ecg, will be shown on
            left of signal plot, defaults to ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']
        lead_order : Lead display order
        columns    : display columns, defaults to 2
        style      : display style, defaults to None, can be 'bw' which means black white
        row_height :   how many grid should a lead signal have,
        show_lead_name : show lead name
        show_grid      : show grid
        show_separate_line  : show separate line
    """

    def normalize_relevances_quartile(relevances, quartile=XAI_quartile):
        """
        Normalize the results of an XAI approach between -1 and 1 using IQR for robustness;
        the input are the relevances indicating which part of the ECG was important for the model's decision making;
        relevances has to have the identical shape like the ECG.
        """
        # Calculate negative and positive quartile
        quartile_neg = np.percentile(relevances, quartile)
        quartile_pos = np.percentile(relevances, 100-quartile)
        abs_quartile = np.array([abs(quartile_neg), quartile_pos]).max()
        # Apply normalization based on absolute quartile
        normalized_relevances = relevances/ abs_quartile
        normalized_relevances = np.clip(normalized_relevances, -1, 1)
        return normalized_relevances
    #normalize the relevances of XAI
    if XAI_normalize:
        XAI = normalize_relevances_quartile(XAI)
    # Use to show only parts of the relevances
    if XAI_show_just:
        if XAI_show_just == 1 or XAI_show_just == 'positive':
            negative_mask = XAI < 0
            XAI[negative_mask] = 0
        elif XAI_show_just == 'negative':
            positive_mask = XAI > 0
            XAI[positive_mask] = 0
        else:
            print("XAI_show_just expects 'positive'/1 or 'negative'/0 as input.")
    #define colors
    color_contradicting_disease = '#008080' #teal
    color_indicating_disease = '#FF66CC' #rosa
    if XAI_colors:
        if XAI_colors == 1:
            color_contradicting_disease = '#98FB98' #soft gree
            color_indicating_disease = '#0000FF' #vivid blue
        elif XAI_colors ==2:
            color_contradicting_disease = '#E69F00' #vermilion
            color_indicating_disease = '#56B4E9' #sky blue
        elif XAI_colors ==3:
            color_contradicting_disease = '#6AA84F' #desaturated green
            color_indicating_disease = '#E06666' #dark salmon
        elif XAI_colors ==4:
            color_contradicting_disease = '#708090' #slate grey
            color_indicating_disease = '#FFDB58' #mustard yellow
        else:
            color_contradicting_disease = XAI_colors[0] #custom color
            color_indicating_disease =  XAI_colors[1] #custom color
    if not lead_order:
        lead_order = list(range(0,len(ecg)))
    secs  = len(ecg[0])/sample_rate
    leads = len(lead_order)
    rows  = int(ceil(leads/columns))
    # display_factor = 2.5
    display_factor = 1
    line_width = 0.5
    fig, ax = plt.subplots(figsize=(secs*columns * display_factor, rows * row_height / 5 * display_factor))
    display_factor = display_factor ** 0.5
    fig.subplots_adjust(
        hspace = 0,
        wspace = 0,
        left   = 0,  # the left side of the subplots of the figure
        right  = 1,  # the right side of the subplots of the figure
        bottom = 0,  # the bottom of the subplots of the figure
        top    = 1
        )

    fig.suptitle(title)

    # Remove axis labeling
    ax.set_xticklabels([])
    ax.set_yticklabels([])

    x_min = 0
    x_max = columns*secs
    y_min = row_height/4 - (rows/2)*row_height
    y_max = row_height/4

    if (style == 'bw'):
        color_major = (0.4,0.4,0.4)
        color_minor = (0.75, 0.75, 0.75)
        color_line  = (0,0,0)
    else:
        color_major = (1,0,0)
        color_minor = (1, 0.7, 0.7)
        color_line  = (0,0,0.7)

    if(show_grid):
        ax.set_xticks(np.arange(x_min,x_max,0.2))
        ax.set_yticks(np.arange(y_min,y_max,0.5))

        ax.minorticks_on()

        ax.xaxis.set_minor_locator(AutoMinorLocator(5))

        ax.grid(which='major', linestyle='-', linewidth=0.5 * display_factor, color=color_major)
        ax.grid(which='minor', linestyle='-', linewidth=0.5 * display_factor, color=color_minor)

    ax.set_ylim(y_min,y_max)
    ax.set_xlim(x_min,x_max)

    for c in range(0, columns):
        for i in range(0, rows):
            if (c * rows + i < leads):
                y_offset = -(row_height/2) * ceil(i%rows)
                # if (y_offset < -5):
                #     y_offset = y_offset + 0.25

                x_offset = 0
                if(c > 0):
                    x_offset = secs * c
                    if(show_separate_line):
                        ax.plot([x_offset, x_offset], [ecg[t_lead][0] + y_offset - 0.3, ecg[t_lead][0] + y_offset + 0.3], linewidth=line_width * display_factor, color=color_line)


                t_lead = lead_order[c * rows + i]
                step = 1.0/sample_rate
                if(show_lead_name):
                    ax.text(x_offset + 0.07, y_offset - 0.5, lead_index[t_lead], fontsize=9 * display_factor)
                x_steps = np.arange(0, len(ecg[t_lead])*step, step) + x_offset
                ax.plot(
                    x_steps,
                    ecg[t_lead] + y_offset,
                    linewidth=line_width * display_factor,
                    color=color_line
                    )

                #Compute where the baseline of ECG lead is; float between 0 and 1
                baseline = 1-(1/(abs(y_min-y_max)/(y_max+abs(y_offset))))

                for j in range(len(x_steps)):
                    xai_value = XAI[t_lead][j]
                    x_pos = x_steps[j]
                    if xai_value < 0 and abs(xai_value) > XAI_threshold:
                        ax.axvspan(x_pos, x_pos+0.04, ymin=baseline-0.079, ymax=baseline+0.08,color=color_contradicting_disease, alpha=abs(xai_value))
                    if xai_value >=0 and xai_value > XAI_threshold:
                        ax.axvspan(x_pos,x_pos+0.04, ymin=baseline-0.079, ymax=baseline+0.08,color=color_indicating_disease, alpha=xai_value)
